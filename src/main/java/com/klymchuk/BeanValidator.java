package com.klymchuk;

public interface BeanValidator {
    boolean validate();
}
