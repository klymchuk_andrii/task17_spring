package com.klymchuk;

import com.klymchuk.beans.BeanC;
import com.klymchuk.beans.BeanD;
import com.klymchuk.configurations.BeanAConfig;
import com.klymchuk.configurations.BeanBConfig;
import com.klymchuk.configurations.BeanEConfig;
import com.klymchuk.configurations.BeanFConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("classpath:beans.properties")
@Import({BeanBConfig.class, BeanAConfig.class, BeanEConfig.class, BeanFConfig.class})
public class AppConfig {

    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;

    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;

    @Bean(name = "BeanC")
    @DependsOn(value = {"BeanD","BeanB"})
    public BeanC getBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Bean(name = "BeanD")
    public BeanD getBeanD() {
        return new BeanD(nameD, valueD);
    }

}
