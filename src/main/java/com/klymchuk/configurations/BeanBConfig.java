package com.klymchuk.configurations;

import com.klymchuk.beans.BeanB;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;

@Configuration
@PropertySource("classpath:beans.properties")
public class BeanBConfig {
    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;

    @Bean(name = "BeanB", initMethod = "init",destroyMethod = "destroy")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }
}
