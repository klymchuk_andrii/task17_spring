package com.klymchuk.configurations;

import com.klymchuk.beans.BeanF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeanFConfig {
    @Bean(initMethod = "init")
    @Lazy
    public BeanF getBeanF(){
        return new BeanF("Bogdan",62);
    }
}
