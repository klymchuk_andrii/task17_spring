package com.klymchuk.configurations;

import com.klymchuk.beans.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Qualifier;

@Configuration
public class BeanAConfig {
    @Bean(name = "BC")
    public BeanA getBeanAFromBC(BeanB beanB, BeanC beanC){
        return new BeanA(beanB.getName(),beanC.getValue());
    }

    @Bean(name = "BD")
    public BeanA getBeanAFromBD(BeanB beanB, BeanD beanD){
        return new BeanA(beanB.getName(),beanD.getValue());
    }

    @Bean(name = "CD")
    public BeanA getBeanAFromCD(BeanD beanD, BeanC beanC){
        return new BeanA(beanD.getName(),beanC.getValue());
    }

}
