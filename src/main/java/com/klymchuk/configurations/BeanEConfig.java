package com.klymchuk.configurations;

import com.klymchuk.beans.BeanA;
import com.klymchuk.beans.BeanE;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanEConfig {

    @Bean(name = "EFirst")
    public BeanE getBeanE1(@Qualifier("BC") BeanA FromBC){
        return new BeanE(FromBC.getName(),FromBC.getValue());
    }

    @Bean(name = "ESecond")
    public BeanE getBeanE2(@Qualifier("BD") BeanA FromBD){
        return new BeanE(FromBD.getName(),FromBD.getValue());
    }
}
