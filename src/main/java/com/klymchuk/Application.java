package com.klymchuk;

import com.klymchuk.beans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;

public class Application {

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application.class);

        ApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);
        BeanC beanC = context.getBean(BeanC.class);
        logger.info(beanC);

        BeanA beanA = context.getBean("BC",BeanA.class);
        logger.info(beanA);

        BeanE beanE1 = context.getBean("EFirst",BeanE.class);
        BeanE beanE2 = context.getBean("ESecond",BeanE.class);
        logger.info(beanE1);
        logger.info(beanE2);

    }
}
