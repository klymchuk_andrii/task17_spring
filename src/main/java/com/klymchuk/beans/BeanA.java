package com.klymchuk.beans;

import com.klymchuk.BeanValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

    private String name;
    private int value;
    private Logger logger = LogManager.getLogger(BeanA.class);

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        return false;
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Destroy BeanA");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Init BeanA");
    }
}
