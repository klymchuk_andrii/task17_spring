package com.klymchuk.beans;

import com.klymchuk.BeanValidator;

public class BeanB implements BeanValidator {

    private String name;
    private int value;

    public BeanB() {
        System.out.println("BeanB initialized");
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
        System.out.println("BeanB initialized");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    private void init(){
        System.out.println("BeanB init");
    }

    private void destroy(){
        System.out.println("BeanB destroy");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        return false;
    }
}
