package com.klymchuk.beans;

import com.klymchuk.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BeanE implements BeanValidator {

    private String name;
    private int value;
    private Logger logger = LogManager.getLogger(BeanA.class);

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        return false;
    }


    @PostConstruct
    public void init() {
        logger.info("BeanE PostConstruct init");
    }

    @PreDestroy
    public void destroy() {
        logger.info("BeanE PreDestroy");
    }
}
