package com.klymchuk.beans;

import com.klymchuk.BeanValidator;

public class BeanF implements BeanValidator {
    private String name;
    private int value;

    public BeanF() {
    }

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
        System.out.println("BeanF initialized");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    private void init(){
        System.out.println("BeanF init");
    }

    @Override
    public boolean validate() {
        return false;
    }
}
