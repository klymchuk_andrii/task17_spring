package com.klymchuk.beans;

import com.klymchuk.BeanValidator;

public class BeanC implements BeanValidator {
    private String name;
    private int value;

    public BeanC() {
        System.out.println("BeanC initialized");
    }

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
        System.out.println("BeanC initialized");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean validate() {
        return false;
    }
}
